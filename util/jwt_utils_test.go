package util

import (
	"fmt"
	"testing"
)

func TestGenerateToken(t *testing.T) {
	toeken := GenerateToken("xiaoguang")

	fmt.Println(toeken)
}

func TestParseToken(t *testing.T) {
	token := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2NzI5OTUwNDEsImlzcyI6InhpYW9ndWFuZyJ9.iophEhtSDC1XVLel9WdH9pb03-E0VQIqG9h2PrLj22o"
	mapClaims, err := ParseToken(token)
	if err != nil {
		fmt.Println(err)
		return
	}
	for k, v := range mapClaims {
		fmt.Println(k, v)
		if k == "exp" {
			fmt.Printf("%f", v)
		}
	}
}
