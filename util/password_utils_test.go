package util

import (
	"fmt"
	"testing"
)

func TestGeneratePassWd(t *testing.T) {
	s1, _ := GeneratePassWd("denglibin")
	s2, _ := GeneratePassWd("denglibin")
	fmt.Println(s1) //$2a$04$MXY2F02EKoRo2e/UHxk5Ae6pEGxPaKwCVH.vf3Xdrd0JY/xPRgdwm
	fmt.Println(s2) //$2a$04$vOeKWRLVqZAVUbAbC4ODM.v8x5HsSnSTPjGCDxjP8qwoYtu16nUF.
}

func TestValidatePassWd(t *testing.T) {
	b := ValidatePassWd("123456", "$2a$10$s8qXiDfLItZoH/1/ZTZNs./lSVY2P8DaZ80MnkoIcQAbwvxj0sMn6")
	b2 := ValidatePassWd("denglibin", "$2a$04$MXY2F02EKoRo2e/UHxk5Ae6pEGxPaKwCVH.vf3Xdrd0JY/xPRgdwm")
	fmt.Println(b)
	fmt.Println(b2)
}
