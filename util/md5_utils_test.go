package util

import (
	"fmt"
	"strings"
	"testing"
)

func TestMd5(t *testing.T) {
	var str string = "hello"
	s := Md5(&str)
	fmt.Println(s, len(s))
	fmt.Println(strings.ToUpper(s), len(s))
}

func TestFileMd5(t *testing.T) {
	var path string = "d:\\labuladong的算法小抄.pdf"
	s, err := GetFileMd5(&path)
	if err != nil {
		panic(err)
	}
	fmt.Println(s)
}

func TestFileMd5_2(t *testing.T) {
	var path string = "d:\\labuladong的算法小抄.pdf"
	s, err := GetFileMd5_2(&path)
	if err != nil {
		panic(err)
	}
	fmt.Println(s)
}
