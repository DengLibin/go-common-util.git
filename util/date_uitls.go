package util

import (
	"fmt"
	"time"
)

// GetNowDateTimeString 获取当前日期的字符串形式 格式:"2006-01-02 15:04:05"
func GetNowDateTimeString() string {

	return fmt.Sprint(time.Now().Format("2006-01-02 15:04:05"))

}

// ParseLocalTime 解析日期 格式 "2006-01-02 15:04:05"
func ParseLocalTime(timeStr string) (time.Time, error) {
	//time.Parse方法是转为格林威治时间的，也就是0时区，这里使用ParseInLocation
	return time.ParseInLocation("2006-01-02 15:04:05", timeStr, time.Local)

}

// SubSec 计算两个时间相差秒数 time1到time2经过的秒数 time2小于time1则为负数
func SubSec(time1 time.Time, time2 time.Time) float64 {
	return time1.Sub(time2).Seconds()
}

// SubMin time1 -time2
func SubMin(time1 time.Time, time2 time.Time) float64 {
	return time1.Sub(time2).Minutes()
}
