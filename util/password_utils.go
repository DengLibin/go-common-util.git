package util

import (
	"golang.org/x/crypto/bcrypt"
)

// ValidatePassWd 比对用户密码是否相等
func ValidatePassWd(src string, passWd string) bool {
	byteHash := []byte(passWd)
	if err := bcrypt.CompareHashAndPassword(byteHash, []byte(src)); err != nil {

		return false
	}
	return true
}

// GeneratePassWd 加密密码
func GeneratePassWd(src string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(src), bcrypt.MinCost)
	if err != nil {
		return "", err
	}
	return string(bytes), nil
}
