package util

import (
	"fmt"
	"testing"
)

func Test_Httpclient_Down(t *testing.T) {
	err := DownFile("https://goldprice.org/charts/gold_3d_b_o_usd_x.png", "./gold.png")
	if err != nil {
		fmt.Println(err)
	}
}

func Test_Httpclient_Get(t *testing.T) {
	s, err := DoGet("https://baidu.com")
	if err != nil {
		panic(err)
	}
	fmt.Println(s)
}
func Test_Httpclient_PostForm(t *testing.T) {
	var params map[string][]string = make(map[string][]string)
	params["icon"] = []string{"234"}
	s, err := DoPostForm("http://192.168.6.141/api/data-release/listIcon", params)
	if err != nil {
		panic(err)
	}
	fmt.Println(s)
}

func Test_Httpclient_PostRequest(t *testing.T) {

	var header map[string]string = make(map[string]string)
	header["Content-Type"] = "application/json;charset=utf-8"

	var body string = `{"passwords":"123456","phoneNum":"deng","userName":""}`

	s, err := DoPostReqeust("http://192.168.6.141/api/login/auth", header, body)
	if err != nil {
		panic(err)
	}
	fmt.Println(s)
}
func Test_Httpclient_PostJSON(t *testing.T) {

	type User struct {
		PhoneNum  string `json:"phoneNum"`
		Passwords string `json:"passwords"`
	}
	var u User = User{PhoneNum: "deng", Passwords: "123456"}
	s, err := DoPostJSON("http://192.168.6.141/api/login/auth", nil, &u)
	if err != nil {
		panic(err)
	}
	fmt.Println(s)
}
func Test_Httpclient_UploadOneFile(t *testing.T) {
	var header map[string]string = make(map[string]string)
	header["_token"] = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIzNjMyNjg0ODI3NDAzNTciLCJleHAiOjE2NzMwNTcwMDAsImlhdCI6MTY3Mjk3MDYwMH0.LmvGLfLZUQ1kB4fruzdD-pRZxMdjMeRpPPmd7sRXI9bF2KTFiCVjYWS8GWl8GJrPV8eLMdpApv3DkTOnH3_wbw"

	s, err := UploadOneFile("http://192.168.6.141/api/file/upload", header, "file", "D:\\Vert.x应用开发实例教程.pdf", "Vert.x应用开发实例教程.pdf")
	if err != nil {
		panic(err)
	}
	fmt.Println(s)
}
