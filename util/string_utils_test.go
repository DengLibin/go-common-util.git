package util

import (
	"fmt"
	"math/rand"
	"testing"
	"time"
)

func TestGetRandomStr(t *testing.T) {
	var s int64 = time.Now().UnixMilli()
	// for i := 0; i < 10; i++ {
	// 	s = time.Now().UnixMilli()
	// 	time.Sleep(time.Duration(2 * time.Millisecond))
	// 	fmt.Println(s)
	// }
	fmt.Println(s)
	rand.Seed(s)
	for i := 0; i < 10; i++ {
		s := GetRandomStr(10)
		fmt.Println(s)
	}

	for i := 0; i < 10; i++ {
		fmt.Printf("%2.2f ", 100*rand.Float32())
	}
}
