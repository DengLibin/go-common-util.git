package util

import (
	"crypto/md5"
	"encoding/hex"
	"io"
	"os"
)

// Md5 md5 16进制 32位 小写
func Md5(str *string) string {
	h := md5.New()
	h.Write([]byte(*str))
	return hex.EncodeToString(h.Sum(nil))
}

/**
 * @Author: DengLibin
 * @Date: Create in 2023-01-05 16:42:56
 * @Description: 获取文件md5
 */
func GetFileMd5(filepath *string) (string, error) {
	f, err := os.Open(*filepath)
	if err != nil {
		return "", nil
	}

	defer f.Close()

	md5hash := md5.New()
	if _, err := io.Copy(md5hash, f); err != nil {
		return "", err
	}

	return hex.EncodeToString(md5hash.Sum(nil)), nil
}

/**
 * @Author: DengLibin
 * @Date: Create in 2023-01-05 16:30:05
 * @Description: 获取文件md5 16进制 32位 小写
 */
func GetFileMd5_2(filepath *string) (string, error) {
	f, err := os.Open(*filepath)
	if err != nil {

		return "", err
	}

	defer f.Close()

	body, err := io.ReadAll(f)
	if err != nil {

		return "", nil
	}
	var m [16]byte = md5.Sum(body)
	return hex.EncodeToString(m[0:]), nil

}
