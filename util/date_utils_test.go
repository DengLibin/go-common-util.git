package util

import (
	"fmt"
	"testing"
	"time"
)

func TestGetNowDateTimeString(t *testing.T) {
	fmt.Println(GetNowDateTimeString())
}
func TestParseTime(t *testing.T) {
	s, _ := ParseLocalTime("2021-10-19 16:57:16")
	fmt.Println(s)
}

func TestSubSec(f *testing.T) {
	t, _ := ParseLocalTime("2021-10-19 16:57:16")
	s := SubSec(time.Now(), t)
	fmt.Println(s)

	s = SubSec(t, time.Now())
	fmt.Println(s)
}
func TestSubMin(f *testing.T) {
	t, _ := ParseLocalTime("2021-10-19 16:57:16")
	s := SubMin(time.Now(), t)
	fmt.Println(s)

	s = SubMin(t, time.Now())
	fmt.Println(s)
}
