package util

import (
	"bytes"
	"math/rand"
	"strconv"
)

var base string = "abcdefghijklmnopqrstuvwxyz0123456789"

// StrToInt 字符串转Int
func StrToInt(str *string) int {
	i, err := strconv.Atoi(*str)
	if err != nil {
		panic(err)
	}
	return i
}

/*
*
  - @Author: DengLibin
  - @Date: Create in 2023-01-05 17:30:12
  - @Description:  获取随机字符 注意 初始化随机数种子
    var s int64 = time.Now().UnixMilli()
    rand.Seed(s)
*/
func GetRandomStr(length int) string {
	var buffer bytes.Buffer

	l := len(base)
	for i := 0; i < length; i++ {
		r := rand.Intn(l)
		c := base[r]
		buffer.WriteRune(rune(c))
	}
	return buffer.String()
}
