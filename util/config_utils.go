package util

import (
	"encoding/json"
	"os"
)

// LoadJsonConfig 加载json配置文件到结构体
func LoadJsonConfig(filePath string, ptr interface{}) error {
	filePtr, err := os.Open(filePath)
	if err != nil {
		return err
	}
	defer filePtr.Close()
	// 创建json解码器
	decoder := json.NewDecoder(filePtr)
	return decoder.Decode(ptr)

}
