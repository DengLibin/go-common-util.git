module gitee.com/DengLibin/go-common-util.git
go 1.19

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/extrame/xls v0.0.1
	//日志输出到文件
	github.com/lestrrat-go/file-rotatelogs v2.4.0+incompatible
	//logrus钩子（Local Filesystem Hook for Logrus）
	github.com/rifflock/lfshook v0.0.0-20180920164130-b9218ef580f5
	//日志库
	github.com/sirupsen/logrus v1.9.0
	//excel操作 文档:https://xuri.me/excelize/zh-hans/
	github.com/xuri/excelize/v2 v2.6.1
	golang.org/x/crypto v0.5.0
	gorm.io/driver/sqlite v1.4.4
	gorm.io/gorm v1.24.3
)

require (
	github.com/extrame/ole2 v0.0.0-20160812065207-d69429661ad7 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	github.com/jonboulle/clockwork v0.3.0 // indirect
	github.com/lestrrat-go/strftime v1.0.6 // indirect
	github.com/mattn/go-sqlite3 v1.14.15 // indirect
	github.com/mohae/deepcopy v0.0.0-20170929034955-c48cc78d4826 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/richardlehane/mscfb v1.0.4 // indirect
	github.com/richardlehane/msoleps v1.0.3 // indirect
	github.com/stretchr/testify v1.8.0 // indirect
	github.com/xuri/efp v0.0.0-20220603152613-6918739fd470 // indirect
	github.com/xuri/nfp v0.0.0-20220409054826-5e722a1d9e22 // indirect
	golang.org/x/image v0.0.0-20220722155232-062f8c9fd539 // indirect
	golang.org/x/net v0.5.0 // indirect
	golang.org/x/sys v0.4.0 // indirect
	golang.org/x/text v0.6.0 // indirect
	gorm.io/driver/mysql v1.4.5
)

require github.com/go-sql-driver/mysql v1.7.0 // indirect
