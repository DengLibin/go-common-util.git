package office

import (
	"bufio"
	"fmt"
	"os"
	"testing"
)

func TestCreateSingleSheetXlsx(t *testing.T) {
	data := map[string]string{
		//学科
		"B1": "语文",
		"C1": "数学",
		"D1": "英语",
		"E1": "理综",

		//姓名
		"A2": "啊俊",
		"A3": "小杰",
		"A4": "老王",

		//啊俊成绩
		"B2": "112",
		"C2": "115",
		"D2": "128",
		"E2": "255",

		//小杰成绩
		"B3": "100",
		"C3": "90",
		"D3": "110",
		"E3": "200",

		//老王成绩
		"B4": "70",
		"C4": "140",
		"D4": "60",
		"E4": "265",
	}
	filePath := "C:\\Users\\denglibin\\Desktop\\成绩表.xlsx"
	file, err := os.OpenFile(filePath, os.O_CREATE, 0666)
	if err != nil {
		panic(err)
	}
	writer := bufio.NewWriter(file)
	CreateSingleSheetExcel(writer, "成绩表", data)
}

func TestReadXlsx(t *testing.T) {
	filePath := "C:\\Users\\denglibin\\Desktop\\成绩表.xlsx"
	file, err := os.OpenFile(filePath, os.O_RDONLY, 0666)
	if err != nil {
		panic(err)
	}
	reader := bufio.NewReader(file)
	data, err := ReadXlsx(reader)
	if err != nil {
		panic(err)
	}
	for k, v := range data {
		fmt.Println("sheetName:", k)
		for _, row := range v {
			fmt.Println(row)
		}
	}
	err = file.Close()
	if err != nil {
		panic(err)
	}
}

func TestReadXls(t *testing.T) {
	filePath := "C:\\Users\\denglibin\\Desktop\\成绩表.xls"

	data, err := ReadXls(filePath, "utf-8")
	if err != nil {
		panic(err)
	}
	for k, v := range data {
		fmt.Println("sheetName:", k)
		for _, row := range v {
			fmt.Println(row)
		}
	}

}
