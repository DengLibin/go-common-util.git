package gormdb

import (
	"fmt"
	"testing"

	"gorm.io/gorm"
)

type User struct {
	Id   int32  `gorm:"column:ID;primary_key" json:"id"`
	Name string `gorm:"column:NAME" json:"name"`
}

func (u *User) TableName() string {
	return "User"
}

func TestExeSql(t *testing.T) {
	// db, err := InitSliteDb("./user.db")
	db, err := InitMysqlDb("denglibin:denglibin@tcp(192.168.6.128:3307)/denglibin?charset=utf8&parseTime=True&loc=Asia%2FShanghai")
	if err != nil {
		panic(err)
	}

	sql := "create table User(ID INTEGER PRIMARY KEY, NAME TEXT)"
	i, err := ExecuteSql(db, true, sql)

	if err != nil {
		panic(err)
	}
	fmt.Println(i)
}

func TestInsertOne(t *testing.T) {
	// db, err := InitSliteDb("./user.db")
	db, err := InitMysqlDb("denglibin:denglibin@tcp(192.168.6.128:3307)/denglibin?charset=utf8&parseTime=True&loc=Asia%2FShanghai")
	if err != nil {
		panic(err)
	}
	user := User{Id: 1, Name: "李四"}
	i, err := InsertOne(db, &user)

	if err != nil {
		panic(err)
	}
	fmt.Println(i)
}
func TestInsertBatch(t *testing.T) {
	// db, err := InitSliteDb("./user.db")
	db, err := InitMysqlDb("denglibin:denglibin@tcp(192.168.6.128:3307)/denglibin?charset=utf8&parseTime=True&loc=Asia%2FShanghai")
	if err != nil {
		panic(err)
	}
	user1 := User{Id: 5, Name: "李四"}
	user2 := User{Id: 6, Name: "王五"}
	user3 := User{Id: 7, Name: "陈易"}
	slice := []User{user1, user2, user3}
	i, err := InsertBatch(db, user1.TableName(), slice)

	if err != nil {
		panic(err)
	}
	fmt.Println(i)
}

func TestGetById(t *testing.T) {
	// db, err := InitSliteDb("./user.db")
	db, err := InitMysqlDb("denglibin:denglibin@tcp(192.168.6.128:3307)/denglibin?charset=utf8&parseTime=True&loc=Asia%2FShanghai")
	if err != nil {
		panic(err)
	}
	user := User{}
	_, err = QueryById(db, 5, &user)
	if err != nil {
		panic(err)
	}
	fmt.Println(&user)
}
func TestGetByIds(t *testing.T) {
	// db, err := InitSliteDb("./user.db")
	db, err := InitMysqlDb("denglibin:denglibin@tcp(192.168.6.128:3307)/denglibin?charset=utf8&parseTime=True&loc=Asia%2FShanghai")
	if err != nil {
		panic(err)
	}
	users := make([]User, 0, 10)
	_, err = QueryByIds(db, "user", &users, []int{2, 3, 4, 5, 6})
	if err != nil {
		panic(err)
	}
	fmt.Println(users)
}

func TestGetAll(t *testing.T) {
	// db, err := InitSliteDb("./user.db")
	db, err := InitMysqlDb("denglibin:denglibin@tcp(192.168.6.128:3307)/denglibin?charset=utf8&parseTime=True&loc=Asia%2FShanghai")
	if err != nil {
		panic(err)
	}
	users := make([]User, 0, 10)
	_, err = QueryAll(db, "user", &users)
	if err != nil {
		panic(err)
	}
	fmt.Println(users)
}

func TestQueryByMap(t *testing.T) {
	db, err := InitSliteDb("./user.db")
	if err != nil {
		panic(err)
	}
	users := make([]User, 0, 10)
	params := make(map[string]interface{}, 10)
	params["name"] = "李四"
	_, err = QueryByMap(db, "user", &users, params)
	if err != nil {
		panic(err)
	}
	fmt.Println(users)
}

func TestQueryPage(t *testing.T) {
	db, err := InitSliteDb("./user.db")
	if err != nil {
		panic(err)
	}
	users := make([]User, 0, 10)
	params := make(map[string]interface{}, 10)
	params["name"] = "李四"
	total, err := QueryPage(db, "user", &users, 2, 2)
	if err != nil {
		panic(err)
	}
	fmt.Println(total, users)
}
func TestQueryBySql(t *testing.T) {
	db, err := InitSliteDb("./user.db")
	if err != nil {
		panic(err)
	}
	users := make([]User, 0, 10)
	_, err = QueryBySql(db, &users, "select id,name from user where id=?", 5)
	if err != nil {
		panic(err)
	}
	fmt.Println(users)
}

func TestQueryPageBySql(t *testing.T) {
	db, err := InitSliteDb("./user.db")
	if err != nil {
		panic(err)
	}
	users := make([]User, 0, 10)
	total, err := QueryPageBySqlUseLimitOffset(db, &users, 1, 3, "select id,name from user where name like ?", "%李%")
	if err != nil {
		panic(err)
	}
	fmt.Println(total, users)
}

func TestUpdateById(t *testing.T) {
	db, err := InitSliteDb("./user.db")
	if err != nil {
		panic(err)
	}
	user := User{Name: "王武"}
	a, err := UpdateById(db, &user, "ID", 1)
	if err != nil {
		panic(err)
	}
	fmt.Println(a)
}

func TestUpdateByMap(t *testing.T) {
	db, err := InitSliteDb("./user.db")
	if err != nil {
		panic(err)
	}
	params := make(map[string]interface{}, 10)
	params["ID"] = 2
	user := User{Name: "王二小"}
	a, err := UpdateByParam(db, &user, params)
	if err != nil {
		panic(err)
	}
	fmt.Println(a)
}
func TestDeleteById(t *testing.T) {
	db, err := InitSliteDb("./user.db")
	if err != nil {
		panic(err)
	}
	user := User{Id: 2}
	a, err := DeleteById(db, &user)
	if err != nil {
		panic(err)
	}
	fmt.Println(a)
}

func TestExecInTx(t *testing.T) {
	db, err := InitSliteDb("./user.db")
	if err != nil {
		panic(err)
	}
	//在事务中执行
	err = ExecInTx(db, func(tx *gorm.DB) error {

		//在事务中执行 使用tx
		params := make(map[string]interface{}, 10)
		params["ID"] = 1
		user := User{Name: "王大"}
		_, err := UpdateByParam(tx, &user, params)
		if err != nil {
			return err
		}
		user.Name = "陆逊"
		_, err = UpdateById(tx, &user, "ID", 1)
		return err
	})
	if err != nil {
		panic(err)
	}
}
