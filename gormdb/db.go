/**
 * @Author: DengLibin
 * @Date: Create in 2023-01-06 15:38:43
 * @Description: db连接
 */
package gormdb

import (
	"gorm.io/driver/mysql"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

/**
 * @Author: DengLibin
 * @Date: Create in 2023-01-06 15:39:12
 * @Description: 使用sqlite数据库
 */
func InitSliteDb(dbPath string) (db *gorm.DB, err error) {
	var dialector gorm.Dialector = sqlite.Open(dbPath)
	return gorm.Open(dialector, &gorm.Config{
		CreateBatchSize:        100, //指定批量插入时 批次大小
		Logger:                 logger.Default.LogMode(logger.Info),
		SkipDefaultTransaction: false, //对于写操作（创建、更新、删除），为了确保数据的完整性，GORM 会将它们封装在事务内运行。但这会降低性能，你可以在初始化时禁用这种方式
		PrepareStmt:            true,  //执行任何 SQL 时都创建并缓存预编译语句，可以提高后续的调用速度
	})
}

/**
 * @Author: DengLibin
 * @Date: Create in 2023-01-09 11:51:13
 * @Description:
 * @param url "root:denglibin@tcp(192.168.56.101:3306)/denglibin?charset=utf8&parseTime=True&loc=Asia%2FShanghai"
 */
func InitMysqlDb(url string) (db *gorm.DB, err error) {
	var dialector gorm.Dialector = mysql.Open(url)
	return gorm.Open(dialector, &gorm.Config{
		CreateBatchSize:        100, //指定批量插入时 批次大小
		Logger:                 logger.Default.LogMode(logger.Info),
		SkipDefaultTransaction: true, //对于写操作（创建、更新、删除），为了确保数据的完整性，GORM 会将它们封装在事务内运行。但这会降低性能，你可以在初始化时禁用这种方式
		PrepareStmt:            true, //执行任何 SQL 时都创建并缓存预编译语句，可以提高后续的调用速度
	})

}
